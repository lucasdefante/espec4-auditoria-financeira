package br.com.itau.cadastro.repositories;

import br.com.itau.cadastro.models.Empresa;
import org.springframework.data.repository.CrudRepository;

public interface EmpresaRepository extends CrudRepository<Empresa, Integer> {

    Iterable<Empresa> findAllByCnpj(String cnpj);
}
