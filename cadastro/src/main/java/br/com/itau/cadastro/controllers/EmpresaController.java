package br.com.itau.cadastro.controllers;

import br.com.itau.cadastro.models.Empresa;
import br.com.itau.cadastro.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cadastro")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    public Empresa cadastrarEmpresa(@RequestBody Empresa empresa) {
        return empresaService.cadastrarEmpresa(empresa);
    }

    @GetMapping("/{cnpj}")
    public List<Empresa> consultarEmpresa(@PathVariable String cnpj) {
        return empresaService.consultarEmpresa(cnpj);
    }
}
