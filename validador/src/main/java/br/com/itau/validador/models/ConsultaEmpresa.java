package br.com.itau.validador.models;

import java.math.BigDecimal;

public class ConsultaEmpresa {

    private BigDecimal capital_social;

    public ConsultaEmpresa() {
    }

    public BigDecimal getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(BigDecimal capital_social) {
        this.capital_social = capital_social;
    }

    @Override
    public String toString() {
        return "ConsultaEmpresa{" +
                "capital_social=" + capital_social +
                '}';
    }
}
