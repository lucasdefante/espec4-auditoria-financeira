package br.com.itau.validador.models;

import br.com.itau.cadastro.models.Empresa;
import org.springframework.stereotype.Component;

@Component
public class EmpresaMapper {

    public EmpresaConsolida toEmpresaConsolida(Empresa empresa, ConsultaEmpresa consultaEmpresa) {
        EmpresaConsolida empresaConsolida = new EmpresaConsolida();
        empresaConsolida.setId(empresa.getId());
        empresaConsolida.setCnpj(empresa.getCnpj());
        empresaConsolida.setTimestamp(empresa.getTimestamp());
        empresaConsolida.setCapital_social(consultaEmpresa.getCapital_social());
        return empresaConsolida;
    }
}
