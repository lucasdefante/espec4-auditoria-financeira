package br.com.itau.consolida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsolidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsolidaApplication.class, args);
	}

}
