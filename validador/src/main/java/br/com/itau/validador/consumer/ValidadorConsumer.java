package br.com.itau.validador.consumer;

import br.com.itau.cadastro.models.Empresa;
import br.com.itau.validador.clients.ReceitaClient;
import br.com.itau.validador.models.ConsultaEmpresa;
import br.com.itau.validador.services.ValidadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ValidadorConsumer {

    @Autowired
    private ReceitaClient receitaClient;

    @Autowired
    private ValidadorService validadorService;

    @KafkaListener(topics = "spec4-lucas-defante-1", groupId = "dev")
    public void receber(@Payload Empresa empresa) throws Exception {
        ConsultaEmpresa consultaEmpresa = receitaClient.consultarCnpj(empresa.getCnpj());
        System.out.println(empresa.toString() + " - " + consultaEmpresa.toString());
        validadorService.validarEmpresa(empresa, consultaEmpresa);
    }
}
