package br.com.itau.validador.models;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class EmpresaConsolida {

    private int id;
    private String cnpj;
    private Timestamp timestamp;
    private BigDecimal capital_social;

    public EmpresaConsolida() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public BigDecimal getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(BigDecimal capital_social) {
        this.capital_social = capital_social;
    }

    @Override
    public String toString() {
        return "EmpresaConsolida{" +
                "id=" + id +
                ", cnpj='" + cnpj + '\'' +
                ", timestamp=" + timestamp +
                ", capital_social=" + capital_social +
                '}';
    }
}
