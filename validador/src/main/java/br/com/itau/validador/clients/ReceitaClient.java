package br.com.itau.validador.clients;

import br.com.itau.validador.models.ConsultaEmpresa;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "RECEITA", url = "https://www.receitaws.com.br/v1")
public interface ReceitaClient {

    @GetMapping("/cnpj/{cnpj}")
    public ConsultaEmpresa consultarCnpj(@PathVariable String cnpj);
}
