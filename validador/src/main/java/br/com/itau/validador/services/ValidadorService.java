package br.com.itau.validador.services;

import br.com.itau.cadastro.models.Empresa;
import br.com.itau.validador.models.ConsultaEmpresa;
import br.com.itau.validador.models.EmpresaConsolida;
import br.com.itau.validador.models.EmpresaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class ValidadorService {

    @Autowired
    private KafkaTemplate<String, EmpresaConsolida> producer;

    @Autowired
    private EmpresaMapper empresaMapper;

    private static final String TOPICO = "spec4-lucas-defante-3";

    public void validarEmpresa(Empresa empresa, ConsultaEmpresa consultaEmpresa) {
        Random rnd = new Random();
        if(consultaEmpresa.getCapital_social().doubleValue() > 1000000000.0) {
            EmpresaConsolida empresaConsolida = empresaMapper.toEmpresaConsolida(empresa, consultaEmpresa);
            producer.send(TOPICO, empresaConsolida);
            System.out.println("Cadastro aceito.\n");
        } else {
            System.out.println("Cadastro recusado.\n");
        }
    }
}
