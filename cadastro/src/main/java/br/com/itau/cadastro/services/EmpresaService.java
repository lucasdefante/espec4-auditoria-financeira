package br.com.itau.cadastro.services;

import br.com.itau.cadastro.models.Empresa;
import br.com.itau.cadastro.repositories.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaRepository empresaRepository;

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    private static final String TOPICO = "spec4-lucas-defante-1";

    public Empresa cadastrarEmpresa(Empresa empresa) {
        Empresa empresaDB = empresaRepository.save(empresa);
        Random rnd = new Random();
        enviarAoKafka(TOPICO, empresaDB);

        return empresaDB;
    }

    private void enviarAoKafka(String topico, Empresa empresa) {
        producer.send(topico, empresa);
    }

    public List<Empresa> consultarEmpresa(String cnpj) {
        return (List) empresaRepository.findAllByCnpj(cnpj);
    }
}
