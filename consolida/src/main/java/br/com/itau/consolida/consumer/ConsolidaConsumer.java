package br.com.itau.consolida.consumer;

import br.com.itau.consolida.io.CSVHandler;
import br.com.itau.validador.models.EmpresaConsolida;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class ConsolidaConsumer {

    private static final Path path = Paths.get(System.getProperty("user.home"),"cadastroEmpresas.csv");

    @KafkaListener(topics = "spec4-lucas-defante-3", groupId = "dev")
    public void receber(@Payload EmpresaConsolida empresaConsolida) throws Exception {
        System.out.println("Cadastro recebido: " + empresaConsolida.toString());
        CSVHandler.csvWriterOneByOne(empresaConsolida, path);
    }
}
